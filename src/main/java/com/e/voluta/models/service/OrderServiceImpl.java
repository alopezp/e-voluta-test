package com.e.voluta.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e.voluta.dto.OrderDto;
import com.e.voluta.models.dao.IOrder;
import com.e.voluta.models.dao.IOrderLine;
import com.e.voluta.models.entity.Order;
import com.e.voluta.models.entity.OrderLine;

@Service
public class OrderServiceImpl implements IOrderService {
	@Autowired
	private IOrder orderDao;

	@Autowired
	private IOrderLine orderLineDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Order> findAll() {
		// Regresamos el total de ordenes
		return (List<Order>) orderDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Order findById(Long id) {
		// Regresamos una orden en específico
		return orderDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Order save(Order order) {
		// Guradamos una orden y su detalle del pedido
		Order orderInstance = null;
		if(order.getOrderId() == null) {
			orderInstance = new Order();
			orderInstance.setEmail(order.getEmail());
			orderInstance.setFirstName(order.getFirstName());
			orderInstance.setLastName(order.getLastName());
			orderDao.save(orderInstance);
		}
		if(!(order.getOrderesLines() == null)) {
			for(OrderLine orderLine: order.getOrderesLines()) {
				if(orderLine.getOrderLineId()==null) {
					orderLine.setOrder(orderInstance == null ? order : orderInstance);
					orderLineDao.save(orderLine);
				}else {
					OrderLine orderLineInstance = orderLineDao.findById(orderLine.getOrderLineId()).orElse(null);
					if(!(orderLineInstance==null)) {
						orderLineInstance = orderLineDao.save(orderLine);
					}
				}
			}
		}
		return orderInstance == null ? order : orderInstance;
	}

	/**Método para calcular las sumas de los totales
	 * **/
	@Override
	public void getTotal(OrderDto orderDto, Order orderInstance) {
		if(orderInstance.getOrderesLines() != null && orderInstance.getOrderesLines().size() > 0) {
			orderInstance.getOrderesLines().forEach(ol -> {
				/** Multiplicamos precio por cantidad de artículos y sumamos todas los pedidos
				  **/
				orderDto.setTotalPrice((orderDto.getTotalPrice()== null) ? 0 + (ol.getPrice() * ol.getUnit()) 
						: orderDto.getTotalPrice() +  (ol.getPrice() * ol.getUnit() )); 
			});
			orderDto.setTotalOrder(orderInstance.getOrderesLines().size());
		}
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Order order = findById(id);
		if(order != null) {
			orderDao.delete(order);
		}
	}
}
