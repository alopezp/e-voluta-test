package com.e.voluta.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.e.voluta.models.dao.IOrderLine;
import com.e.voluta.models.entity.OrderLine;

@Service
public class OrserLineServiceImpl implements IOrderLineService {

	@Autowired
	private IOrderLine orderLineDao;
	
	@Override
	public List<OrderLine> findAll() {
		// TODO Auto-generated method stub
		return (List<OrderLine>) orderLineDao.findAll();
	}

	@Override
	public OrderLine findById(Long id) {
		// TODO Auto-generated method stub
		return orderLineDao.findById(id).orElse(null);
	}

	@Override
	public OrderLine save(OrderLine orderLine) {
		// TODO Auto-generated method stub
		return orderLineDao.save(orderLine);
	}

}
