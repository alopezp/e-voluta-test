package com.e.voluta.models.service;

import java.util.List;

import com.e.voluta.dto.OrderDto;
import com.e.voluta.models.entity.Order;

public interface IOrderService {
	
	public List<Order> findAll();
	public Order findById(Long id);
	public Order save(Order order);
	public void getTotal(OrderDto orderDto, Order order);
	public void delete(Long id);
}
