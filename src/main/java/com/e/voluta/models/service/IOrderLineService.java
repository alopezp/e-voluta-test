package com.e.voluta.models.service;

import java.util.List;

import com.e.voluta.models.entity.OrderLine;

public interface IOrderLineService {

	public List<OrderLine> findAll();
	public OrderLine findById(Long id);
	public OrderLine save(OrderLine orderLine);
}
