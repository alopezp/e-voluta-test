package com.e.voluta.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.e.voluta.models.entity.Order;

public interface IOrder extends CrudRepository<Order, Long> {

}
