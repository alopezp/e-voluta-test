package com.e.voluta.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.e.voluta.models.entity.OrderLine;

public interface IOrderLine extends CrudRepository<OrderLine, Long> {

}
