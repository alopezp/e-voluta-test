package com.e.voluta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EVolutaTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(EVolutaTestApplication.class, args);
	}

}
