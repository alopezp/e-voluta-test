package com.e.voluta.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.e.voluta.dto.OrderDto;
public interface IOrderController {
	public List<OrderDto> orders();
	public ResponseEntity<?> show(@PathVariable Long id);
	public ResponseEntity<?> create(@Valid @RequestBody OrderDto orderDto);
	public ResponseEntity<?> update(@Valid @RequestBody OrderDto orderDto);
	public ResponseEntity<?> delete(@PathVariable Long id);
}
