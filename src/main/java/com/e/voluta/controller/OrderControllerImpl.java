package com.e.voluta.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.e.voluta.models.entity.Order;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.e.voluta.dto.OrderDto;
import com.e.voluta.models.service.IOrderService;

@RestController
@RequestMapping(path="order")
public class OrderControllerImpl implements IOrderController {
	@Autowired
	private IOrderService orderService;

	private final  ModelMapper modelMapper = new ModelMapper();
	
	@Override
	@GetMapping("/list")
	public List<OrderDto> orders(){
		
		 List<Order> orders = orderService.findAll();
		 if(orders != null) {
			 List<OrderDto> orderDtoList = new ArrayList<OrderDto>();
			 orders.forEach(o -> {
				OrderDto orderDto = modelMapper.map(o, OrderDto.class);
				orderService.getTotal(orderDto, o);
				orderDtoList.add(orderDto);
			 });
			 return orderDtoList;
		 }
		return null;
	}
	
	@Override
	@GetMapping("/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		OrderDto orderDto = null;
		Map<String, Object> response = new HashMap<>();
		try {
			Order order = orderService.findById(id);
			if(order != null) {
				orderDto =  modelMapper.map(order, OrderDto.class);
				orderService.getTotal(orderDto, order);
			}
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(orderDto == null) {
			response.put("mensaje", "Orden no encontrada");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<OrderDto>(orderDto, HttpStatus.OK);
	}
	
	@Override
	@PostMapping("/save")
	public ResponseEntity<?> create(@Valid @RequestBody OrderDto orderDto) {
		Order order = null;
		Map<String, Object> response = new HashMap<>();
		try {
			order = modelMapper.map(orderDto, Order.class);
			order = orderService.save(order);
			orderDto = modelMapper.map(order, OrderDto.class);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al guardar.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Registro creado con éxito");
		response.put("order", orderDto);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@Override
	@PutMapping("/update")
	public ResponseEntity<?> update(@Valid @RequestBody OrderDto orderDto) {
		Map<String, Object> response = new HashMap<>();
		Order order = orderService.findById(orderDto.getOrderId());
		if(!(order== null)) {
			try {
				order = modelMapper.map(orderDto, Order.class);
				order = orderService.save(order);
				orderDto = modelMapper.map(order, OrderDto.class);
			} catch(DataAccessException e) {
				response.put("mensaje", "Error al guardar.");
				response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			response.put("mensaje", "Registro actualizado con éxito");
			response.put("order", orderDto);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		}
		response.put("mensaje", "Error al guardar.");
		response.put("error", "El registro no existe");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			orderService.delete(id);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al eliminar.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Registro eliminado con éxito");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
}
