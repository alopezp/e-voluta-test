package com.e.voluta.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;


public class OrderDto {

	private Long orderId;
	private List<OrderLineDto> orderesLines;
	private String firstName;
	private String lastName;
	private String email;
	private Double totalPrice;
	private int totalOrder;
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public List<OrderLineDto> getOrderesLines() {
		return orderesLines;
	}
	public void setOrderesLines(List<OrderLineDto> orderesLines) {
		this.orderesLines = orderesLines;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public int getTotalOrder() {
		return totalOrder;
	}
	public void setTotalOrder(int totalOrder) {
		this.totalOrder = totalOrder;
	}
}
