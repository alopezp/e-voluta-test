package com.e.voluta.models.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.e.voluta.models.dao.IOrder;
import com.e.voluta.models.entity.Order;


@SpringBootTest
class OrderServiceImplTest {
	 @Mock
	 private IOrder orderDao; 
	 
	 @InjectMocks
	 private OrderServiceImpl orderService;

	@Test
	void create() {
		Order order = new Order();
		order.setOrderId(1L);
		order.setEmail("test@email.com");
		order.setFirstName("Juan");
		order.setLastName("Perez");
		
		Order oderResponse = orderService.save(order);
		
		Assertions.assertEquals(1L, oderResponse.getOrderId());
	    Assertions.assertEquals("Juan", oderResponse.getFirstName());
	    Assertions.assertEquals("Perez", oderResponse.getLastName());
	}
	
	@Test
	void list() {
		Order order = new Order();
		order.setOrderId(1L);
		order.setEmail("test@email.com");
		order.setFirstName("Juan");
		order.setLastName("Perez");
			
		Order oderResponse = orderService.save(order);
		List<Order> orderList  = new ArrayList<Order>();
		orderList.add(oderResponse);
		when(orderService.findAll()).thenReturn(orderList);

		Assertions.assertEquals(1, orderService.findAll().size());
	}
	
	@Test
	void search() {
		Order order = new Order();
		order.setOrderId(1L);
		order.setEmail("test@email.com");
		order.setFirstName("Juan");
		order.setLastName("Perez");
		
		when(orderDao.findById(1L)).thenReturn(Optional.of(order));

		
		Order oderResponse  = orderService.findById(1L);
		System.out.println(oderResponse.getEmail());
		System.out.println("out");
		
		Assertions.assertEquals(1L, oderResponse.getOrderId());
	    Assertions.assertEquals("Juan", oderResponse.getFirstName());
	    Assertions.assertEquals("Perez", oderResponse.getLastName());
	}
	
	@Test
	void delete() {
		Order order = new Order();
		order.setOrderId(1L);
		order.setEmail("test@email.com");
		order.setFirstName("Juan");
		order.setLastName("Perez");

		
		when(orderDao.findById(order.getOrderId())).thenReturn(Optional.of(order));
		orderService.delete(order.getOrderId());
		verify(orderDao).delete(order);
	}

}
